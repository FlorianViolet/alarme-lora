# Compte Rendu Projet IoT Alarme connectée

Violet Florian  
Sui Bing  

## Architecture matérielle de l’objet

L'objet est constitué de la carte Lora-E5-Dev, d'un buzzer, d'un capteur (ici nous utilisons le capteur de température intégré) et d'une LED située sur la carte.

## Communication, forme des messages et sécurité

L'objet est défini comme étant de classe A dans le code présent, mais l'étude de durée de vie de la batterie montre que la classe B est raisonnable. Ainsi dans cet exemple il ne peut recevoir de message qu'après une émission.
Il communique avec un serveur, ChirpStack.

Dans le code présent les données transitent par des chaines de caractères en base64, mais pour des raisons d'économies d'énergie le produit final verrait des messages transmis par payloads LPP (Low Power Payload) où seule la donnée utile est transmise (en plus de son enveloppe).

Le protocole de chiffrement utilisé est OTAA.

[OTAA](https://connect.ed-diamond.com/MISC/MISCHS-015/LORAWAN-deploiement-d-une-infrastructure-de-test) (Over The Air Activation) : dans ce mode, une simple clé AppKey est nécessaire à la configuration du nœud. Elle doit être aussi configurée dans le serveur d’application. Lors du démarrage du nœud, cette clé sera utilisée au cours d’un premier échange de messages Join-Request et Join-Accept pour générer des clés de session NwkSKey et AppSKey qui seront ensuite utilisées pour chiffrer les données échangées ; ces clés de session sont conservées jusqu’à leur réinitialisation. Lors de cette étape, le nœud envoie un aléa de 16 bits qui sera utilisé pour dériver les clés de sessions. Un paquet Join-Accept sera renvoyé à destination du nœud, chiffré avec l’AppKey et contenant notamment l’adresse DevAddr attribuée automatiquement ainsi qu’un aléa de 24 bits utilisé pour dériver les clefs de sessions.

La configuration sur ChirpStack est constituée d'une DevEUI (numéro qui identifie l'appareil) d'une AppKey (la clef qui sert à chiffrer la communication). Une AppEUI est aussi présente dans la partie initialisation du code, mais nous ne nous en servons pas. Ce numéro permet d'itentifier l'application cible à même de traiter les requetes d'accès. Ces numéros sont stockés sur l'appareil et le serveur.

## Architecture logicielle de l’objet

Le logiciel est construit sur la base de l'utilisation d'un OS temps réel Riot et de ses bibliothèques qui nous permettent une abstraction de la gestion du chiffrement des messages et des protocoles d'émission.
Ainsi le logiciel commence par un programme principal qui initialise les éléments nécessaire :

- Les GPIOs de la carte pour gérer buzzer et LED
- Les interruptions pour les boutons
- Le capteur et sa configuration
- Les identifiants nécessaires pour communiquer (DevEUI, AppKey, AppEUI)
- La configuration de la communication (data rate/spreading factor)
- La connexion à ChirpStack
- Le lancement des différents threads

Ces threads permettent d'utiliser les propriétés de l'OS temps réel pour s'assurer une mesure et émission des données à période constante ainsi que la réception des messages et la gestion de l'état de l'objet (alarme déclenchée ou non) en parallèle.
Il y a donc un thread pour chacune de ces actions :

- Prendre des mesures et envoyer les données
- Recevoir les messages
- Gérer la LED (clignotement)
- Gérer le buzzer (abscence de timer matériel)

## Métriques logiciel

Le binaire embarqué mesure 48.9 ko, et le code créé (hors RiotOS utilisé) mesure environ 250 lignes.

## Changements de comportement de l’objet en fonction des événements (normal et incident détecté)

Le système possède deux états : Alarme déclenchée ou non.  
Pour déclencher l'alarme ou la réinitialiser il y a 3 façons :

- Appuyer sur le bouton d'alarme/annulation
- Recevoir un message d'alarme/annulation
- La mesure du capteur

Le bouton d'alarme sur la carte est **_D0_** et celui de retour à la normale est **_Boot_**.
Les messages à envoyer en base64 sont `YWxhcm1fb24=` soit `alarm_on` pour déclencher l'alarme et `YWxhcm1fb2Zm` soit `alarm_off` pour un retour à la normale.
Pour certains seuils de mesure du capteur, l'alarme se retrouvera déclenchée ou réinitialisée.

## Problèmes rencontrés

La carte utilisée ne disposant pas de timer matériel il a fallu en faire un de façon logicielle pour piloter le buzzer. Quand on a testé une fréquence trop grande la tâche de gestion du buzzer prenait tout le temps processeur et les autres tâches ne répondaient plus.

Parfois et de façon imprévisible, l'envoie d'un message reste bloqué indéfiniement sans timeout. La raison est inconnue. Le blocage s'effectue au cours d'un appel à la fonction `semtech_loramac_send`.

## Coûts

La partie coût tient compte du prix de vente au détail pour un particulier et ne prend pas en compte le prix de la boîte, d'assemblage, d'emballage, de transport, et autres détails. Voici quelques-uns des prix les plus représentatifs que nous avons trouvés :
|||
|:--|:--|
|Buzzer|1.50€|
|LoRa Development Kit|30€|
|Total|31.50€|

Donc le prix de 5 000 unités est d'environ 5 000*31.50 euros = 157 500€
Après cela, nous avons également besoin de La certification ETSI (Institut européen des normes de télécommunications), et La certification LoRa Alliance, qui consiste à garantir que nos produits peuvent être utilisés légalement et efficacement.

|||
|:--|:--|
|Certification ETSI 1 an|$1000 USD|
|Certification LoRa Alliance 1 an|$10 000 USD|
|Total annuel|$11 000 USD|

Ainsi, notre prix d'établissement est d'environ 157 500€ plus des frais de certification de 11 000€ par an (hors coûts de production). Tous ces coûts manquants de logistique rendent un prix de vente impossible à déterminer sans une connaissance de ces coûts de production, mais nous pouvons de façon assez sereine affirmer que le prix de vente serait supérieur au double du prix des matériaux brut (soit > 63€).

## Durée de vie de la batterie

Il y a 3 classes dans un protocole de communication LoraWan : A, B et C.

**Classe A :** Cette classe a la consommation énergétique la plus faible. Lorsque l'équipement a des données à envoyer il le fait sans contrôle puis il ouvre 2 fenêtres d'écoute successives pour des éventuels messages provenant du serveur, les durées recommandées sont de 1 puis 2 secondes. Ces 2 fenêtres sont les seules durant lesquelles le serveur peut envoyer à l'équipement les données qu'il a précédemment stockées à son attention.

**Classe B :** Cette classe permet un compromis entre la consommation énergétique et le besoin en communication bi-directionnelle. Ces équipements ouvrent des fenêtres de réception à des intervalles programmés par des messages périodiques envoyés par le serveur.

**Classe C :** Cette classe a la plus forte consommation énergétique mais permet des communications bi-directionnelles n'étant pas programmées. Les équipements ont une fenêtre d'écoute permanente.

La carte LoRa doit être alimentée en 3V. Pour cela 2 piles AA 1.5V sont utilisées en série. Cette batterie possède une capacite entre 2000 et 3000 mAh.

Nous choisissons le mode de communication SF7, une bande passante de 125 KHz et excluons tous les capteurs inutilisés.
Après s'être rendu compte que la réception ne consomme très peu face à l'émission et la mesure. Nous avons donc choisi la classe B avec une émission toute les 5 minutes (pour un message de 20 bytes), une fenêtre de réception toutes les 30 secondes (d'un message très court dont la donnée utile peut être réduite à 1 bit) et une mesure toutes les 10 secondes (la mesure doit être assez fréquente pour déclencher l'alarme à temps).

![Energy Consumption](./Energy_Consumption.png "Energy Consumption")

## Comparaison de la concurence

|Produit|Avantages|Inconvénients|
|:--|:--|:--|
|R602A LoRaWAN Wireless Siren|Durée de vie de cinq ans, petite taille|Son peu puissant|
|ACS Switch Buzz|Son puissant, faible consommation|Prix élevé|
|LoRa SmartVOX|Son puissant|Consommation d'énergie élevée|

En utilisant des cartes de développement dont le prix est plus élevé il est fort probable que le prix finale de notre produit soit suppérieur à ceux sur le marché. De plus le buzzer utilisé a une puissance d'environ 80dB, ce qui est plus faible que les produits concurrent. En revanche la durée de vie de la batterie est similaire. En effet cette durée dépend principalement de la radio et celle ci possède des performances similaires aux autres produits.
