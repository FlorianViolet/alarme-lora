
#include <stdio.h>
#include <string.h>

#include "thread.h"
#include "msg.h"

#include "shell.h"

// connection au réseau
#include "net/loramac.h"
#include "semtech_loramac.h"

#include "board.h"
#include "xtimer.h"
#include "ztimer.h"
#include "periph/gpio.h"
#include "periph_conf.h"

// capteur température de la carte lora
#include "lm75.h"
#include "lm75_params.h"

// objet global pour la comm
extern semtech_loramac_t loramac;

// capteur
static lm75_t lm75;

/* Device and application informations required for OTAA activation */
static const uint8_t deveui[LORAMAC_DEVEUI_LEN] = { 0x7b, 0x77, 0x0d, 0x5f, 0x23, 0x58, 0xb7, 0x64 };
static const uint8_t appeui[LORAMAC_APPEUI_LEN] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
static const uint8_t appkey[LORAMAC_APPKEY_LEN] = { 0x71, 0x56, 0x2F, 0x9D, 0xC2, 0xC9, 0xFB, 0xD0, 0xD9, 0x4B, 0x58, 0x67, 0xD7, 0xDD, 0x02, 0x31 };

#define LORAMAC_RECV_MSG_QUEUE                   (4U)
static msg_t _loramac_recv_queue[LORAMAC_RECV_MSG_QUEUE];
static char _recv_stack[THREAD_STACKSIZE_DEFAULT];
static char _send_stack[THREAD_STACKSIZE_DEFAULT];
static char _alarm_stack[THREAD_STACKSIZE_DEFAULT];
static char _buzzer_stack[THREAD_STACKSIZE_DEFAULT];

static int alarm_triggered = 0;

static kernel_pid_t pid_alarm_led, pid_buzzer_ring; 

// GPIO_PIN(port, pin)	ex PA5 = (0,5)  PB10 = (1,10)
#define BUZZER_PIN	GPIO_PIN(1, 10)

void trigger_alarm(void* arg) {
	(void)arg;
	alarm_triggered = 1;
	thread_wakeup(pid_alarm_led);
	thread_wakeup(pid_buzzer_ring);
}

void clear_alarm(void* arg) {
	(void)arg;
	alarm_triggered = 0;
}

static void *_wait_recv(void *arg)
{
    msg_init_queue(_loramac_recv_queue, LORAMAC_RECV_MSG_QUEUE);

    (void)arg;
    while (1) {
        /* blocks until something is received */
        switch (semtech_loramac_recv(&loramac)) {
            case SEMTECH_LORAMAC_RX_DATA:
                loramac.rx_data.payload[loramac.rx_data.payload_len] = 0;
				if (strcmp("alarm_on", (char*)loramac.rx_data.payload) == 0) {
					trigger_alarm(NULL);
				}
				else if (strcmp("alarm_off", (char*)loramac.rx_data.payload) == 0) {
					clear_alarm(NULL);
				}
                printf("\e[96mData received: %s, port: %d\e[0m\n", (char *)loramac.rx_data.payload, loramac.rx_data.port);
                break;

            case SEMTECH_LORAMAC_RX_LINK_CHECK:
                printf("Link check information:\n"
                   "  - Demodulation margin: %d\n"
                   "  - Number of gateways: %d\n",
                   loramac.link_chk.demod_margin,
                   loramac.link_chk.nb_gateways);
                break;

            case SEMTECH_LORAMAC_RX_CONFIRMED:
                puts("\e[92mReceived ACK from network\e[0m");
                break;

            case SEMTECH_LORAMAC_TX_SCHEDULE:
                puts("\e[96mThe Network Server has pending data\e[0m");
                break;

            default:
                break;
        }
    }
    return NULL;
}

static void * prendre_et_envoyer_les_mesures(void *arg) {
	(void)arg;
	while (1) {
		/* do some measurements */
        int temperature = 0;
        if (lm75_get_temperature(&lm75, &temperature)!= LM75_SUCCESS) {
            puts("\e[91mCannot read temperature! retrying ...\e[0m");
			xtimer_sleep(1);
			continue;
        }

		// could check the sensor more often than the frequency of message sending to detect accident sooner
		// this would allow to send the mean of the measures

		// foolish condition to show exemple of alarm triggered by sensor
		if (temperature/1000 > 22) {
			trigger_alarm(NULL);
		}
		else if (temperature/1000 < 22) {
			clear_alarm(NULL);
		}

        char message[32];
        sprintf(message, "T:%d.%dC", (temperature / 1000), (temperature % 1000));
        printf("\e[93mSending message '%s'\e[0m\n", message);

        /* send the message here */
		if (semtech_loramac_send(&loramac, (uint8_t *)message, strlen(message)) != SEMTECH_LORAMAC_TX_DONE) {
            printf("\e[91mCannot send message '%s'\e[0m\n", message);
        }
        else {
            printf("\e[93mMessage '%s' sent\e[0m\n", message);
        }

        /* wait 20 seconds between each message */
        xtimer_sleep(20);
    }
	return NULL;
}

static void * alarm_led(void* arg) {
    (void)arg;
    while (1) {
        if (alarm_triggered) {
			puts("\e[91mAlarm !\e[0m");
			LED0_TOGGLE;
        }
		else {
			LED0_OFF;
			thread_sleep();
		}
		xtimer_sleep(1);
    }
	return NULL;
}

// 554 Hz (T/2=903µs  100ms) / 440 Hz (T/2=1136µs  400 ms) 
static void * buzzer_ring(void* arg) {
	(void) arg;
	int cpt = 0;
	bool f554 = true;
	while (1) {
		if (alarm_triggered) {
			// absence de timer hardware => timer software
			if (f554) {
				gpio_set(BUZZER_PIN);
				xtimer_usleep(903);
				gpio_clear(BUZZER_PIN);
				xtimer_usleep(903);
				cpt++;
			} else {
				gpio_set(BUZZER_PIN);
				xtimer_usleep(1136);
				gpio_clear(BUZZER_PIN);
				xtimer_usleep(1136);
				cpt++;
			}
			// gère le changement de fréquence
			if (f554 && cpt > 55) {
				f554 = false;
				cpt = 0;
			} else if (!f554 && cpt > 176) {
				f554 = true;
				cpt = 0;
			}
		}
		else {
			gpio_clear(BUZZER_PIN);
			thread_sleep();
		}
	}
	return NULL;
}


int main(void)
{
	// init GPIOs
	if (gpio_init_int(BTN1_PIN, BTN1_MODE, GPIO_FALLING, trigger_alarm, NULL) < 0) {
        puts("[FAILED] init BTN1!");
        return 1;
    }
    if (gpio_init_int(BTN0_PIN, BTN0_MODE, GPIO_FALLING, clear_alarm, NULL) < 0) {
        puts("[FAILED] init BTN0!");
        return 1;
    }
	if (gpio_init(BUZZER_PIN, GPIO_OUT) < 0) {
		puts("[FAILED] init BUZZER!");
        return 1;
	}

	/* configure the sensor */
	if (lm75_init(&lm75, &lm75_params[0]) != LM75_SUCCESS) {
		puts("Sensor initialization failed");
		return 1;
	}

	/* configure the device parameters */
	semtech_loramac_set_deveui(&loramac, deveui);
	semtech_loramac_set_appeui(&loramac, appeui);
	semtech_loramac_set_appkey(&loramac, appkey);

	/* change datarate to DR5 (SF7/BW125kHz) */
	semtech_loramac_set_dr(&loramac, 5);

	/* start the OTAA join procedure */
	if (semtech_loramac_join(&loramac, LORAMAC_JOIN_OTAA) != SEMTECH_LORAMAC_JOIN_SUCCEEDED) {
		puts("Join procedure failed");
		return 1;
	}
	puts("Join procedure succeeded");

	// Thread pour la réception des messages
	puts("Creating receive thread...");
	thread_create(_recv_stack, sizeof(_recv_stack), THREAD_PRIORITY_MAIN - 1, 0, _wait_recv, NULL, "recv thread");
	puts("Receive thread created !");

	// Thread pour les mesures et envoie des messages
	puts("Creating sending thread...");
	thread_create(_send_stack, sizeof(_send_stack), THREAD_PRIORITY_MAIN - 1, 0, prendre_et_envoyer_les_mesures, NULL, "measure and send thread");
	puts("Sending thread created !");

	// Thread pour la gestion des leds de l'alarme
	puts("Creating alarm_led thread...");
	pid_alarm_led = thread_create(_alarm_stack, sizeof(_alarm_stack), THREAD_PRIORITY_MAIN - 1, 0, alarm_led, NULL, "alarm thread");
	puts("Alarm_led thread created !");

	// Thread pour la gestion du buzzer
	puts("Creating buzzer thread...");
	pid_buzzer_ring = thread_create(_buzzer_stack, sizeof(_buzzer_stack), THREAD_PRIORITY_MAIN - 1, 0, buzzer_ring, NULL, "alarm thread");
	puts("Buzzer thread created !");

	//puts("All up, running the shell now");
	//char line_buf[SHELL_DEFAULT_BUFSIZE];
	//shell_run(NULL, line_buf, SHELL_DEFAULT_BUFSIZE);

	return 0;
}
